package test;

import org.junit.Test;
import java.util.Date;
import static org.junit.Assert.*;
import Character.java;

public class Assert {
    @Test
    public void testAssertions(){
        //set up values to test
        String CharacterFirstName = this.CharacterFirstName;
        String CharacterLastName = this.CharacterLastName;
        String CharacterJob = this.CharacterJob;
        String CharacterHair = this.CharacterHair;
        String CharacterBody = this.CharacterBody;
        Date createDate = this.createDate;
        int CharacterId = this.CharacterId;
        //is the condition not null
        assertNotNull(CharacterFirstName);
        //is the condition not null
        assertNotNull(CharacterLastName);
        //is the condition not null
        assertNotNull(CharacterJob);
        //is the condition not null
        assertNotNull(CharacterHair);
        //is the condition not null
        assertNotNull(CharacterBody);
        //is the condition not null
        assertNotNull(createDate);
        //is the condition not null
        assertNotNull(CharacterId);
        // are the conditions the same
        assertSame(CharacterFirstName,CharacterLastName);

    }
}
